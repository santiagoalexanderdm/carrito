/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.TreeSet;
import java.time.LocalDate;

/**
 *
 * @author estudiante
 */
public class Carrito {
    
    private int id;
    private Cliente myCliente;
    private TreeSet<Item> items=new TreeSet();
    //Debe ser tomada del sistema
    private LocalDate fecha;

    public Carrito(int id, Cliente myCliente, LocalDate fecha) {
        this.id = id;
        this.myCliente = myCliente;
        this.fecha = fecha;
    }

    public Carrito() {
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Cliente getMyCliente() {
        return myCliente;
    }

    public void setMyCliente(Cliente myCliente) {
        this.myCliente = myCliente;
    }

    public TreeSet<Item> getItems() {
        return items;
    }

    public void setItems(TreeSet<Item> items) {
        this.items = items;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    
}
